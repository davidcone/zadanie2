const {useState} = require("react");
const Input = ({value, handleValueChange}) => {
    const [val, setVal] = useState(value)

    const handleInput = (event) => {
        let inputValue = event.target.value;
        if(inputValue < 1000){
            setVal(inputValue)
            handleValueChange(inputValue)
        }
    }
    return (
        <input value={val} type="number"  onInput={handleInput}/>
    )
}
export default Input
