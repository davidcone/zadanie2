import Cell from "./Cell";

const Row = ({rowDataArray, keyRow, rowSum}) => {

    return (
        <div className='row'>
            {
                rowDataArray.map((cellValue,keyCell) => {
                    return (
                        <Cell value={cellValue} keyCell={keyCell} keyRow={keyRow}/>
                    )
                })
            }
            <div className='sum'>{rowSum}</div>
        </div>
    )
}
export default Row
