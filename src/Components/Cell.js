import Input from "./Input";
import {updateMatrix} from "../Store/excelStore";
import {useDispatch} from "react-redux";

const Cell = ({value, keyRow, keyCell}) => {
    const dispatch = useDispatch()
    const handleInput = (keyRow,keyCol,val) => {
        dispatch(updateMatrix({row:keyRow, column: keyCol, value: val}))
    }
    return (
        <div className='cell'>
            <Input value={value} handleValueChange={(value) => handleInput(keyRow,keyCell,value)}/>
        </div>
    )
}
export default Cell
