import {randomInt} from "./randomNumbersMatrixGenerator";

export const calculateSum = (matrix) => {
    let sumArr = [];
    for(let row of matrix){
        sumArr.push(row.reduce((partialSum, a) => partialSum + a, 0))
    }
    return sumArr
}
export const calculateMatrix = (matrix) => {
    return {
        matrix: matrix,
        matrixSize: matrix.length,
        matrixRowSum: calculateSum(matrix)
    }
}
export const increaseMatrix = (matrix) => {
    let matrixSize = matrix.length + 1
    let outputMatrix = []
    // Row operation
    for(let row of matrix){
        let tmpRow =  JSON.parse(JSON.stringify(row))
        // Add cell to row
        tmpRow.push(randomInt())
        // push row to output matrix
        outputMatrix.push(tmpRow)
    }
    // Column operation
    let tmpRow = []
    for(let i = 0; i < matrixSize; i++){
        tmpRow.push(randomInt())
    }
    outputMatrix.push(tmpRow)
    return {
        matrix: outputMatrix,
        matrixSize,
        matrixRowSum: calculateSum(outputMatrix)
    }
}
export const decreaseMatrix = (matrix, minimumMatrixSize = 4) => {
    let outputMatrix = matrix;
    if(matrix.length > minimumMatrixSize){
      let matrixSize = matrix.length - 1;
      outputMatrix = []
      for(let row of matrix){
        let tmpRow = JSON.parse(JSON.stringify(row))
        tmpRow.pop()
        outputMatrix.push(tmpRow)
      }
      outputMatrix.pop()
    }
    return {
        matrix: outputMatrix,
        matrixSize: outputMatrix.length,
        matrixRowSum: calculateSum(outputMatrix)
    }
}
