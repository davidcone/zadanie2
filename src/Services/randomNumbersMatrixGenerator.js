const defaultSize = 4;

export function randomInt(min=0, max=999) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

export function generateMatrix(size = defaultSize){
    let tmpArr = [];
    for(let j = 0; j < size; j++){
        let tmpInnerArr = [];
        for(let j = 0; j <size; j++){
            tmpInnerArr.push(randomInt(0,999))
        }
        tmpArr.push(tmpInnerArr)
    }
    return tmpArr
}
