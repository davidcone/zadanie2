import {createSlice, configureStore} from "@reduxjs/toolkit";
import {generateMatrix} from "../Services/randomNumbersMatrixGenerator";
import {calculateMatrix, decreaseMatrix, increaseMatrix} from "../Services/matrixCalculator";

const defaultMatrixSize = 4;
const generatedMatrix = generateMatrix(defaultMatrixSize);
const initialState = calculateMatrix(generatedMatrix)

const matrixSlice = createSlice({
    name: 'matrix',
    initialState,
    reducers: {
        increaseMatrixSize: state => {
            let {matrix, matrixSize, matrixRowSum} = increaseMatrix(state.matrix)
            state.matrix = matrix;
            state.matrixSize = matrixSize;
            state.matrixRowSum = matrixRowSum
        },
        decreaseMatrixSize: state => {
            let {matrix, matrixSize, matrixRowSum} = decreaseMatrix(state.matrix)
            state.matrix = matrix;
            state.matrixSize = matrixSize;
            state.matrixRowSum = matrixRowSum
        },
        updateMatrix:  (state, action) => {
            let {row,column,value} = action.payload
            state.matrix[row][column] = +value;
            let {matrix, matrixSize, matrixRowSum} = calculateMatrix(state.matrix)
            state.matrix = matrix;
            state.matrixSize = matrixSize;
            state.matrixRowSum = matrixRowSum
        }
    }
})

export const {increaseMatrixSize, decreaseMatrixSize, updateMatrix} = matrixSlice.actions

export const store = configureStore({
    reducer: {
        matrix: matrixSlice.reducer
    }
})

