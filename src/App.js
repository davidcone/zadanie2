import './App.css';
import Input from "./Components/Input";
import {useDispatch, useSelector} from "react-redux";
import {decreaseMatrixSize, increaseMatrixSize, updateMatrix} from "./Store/excelStore";
import Row from "./Components/Row";

function App() {
    const matrix = useSelector(state => state.matrix)
    const dispatch = useDispatch()

    const handleIncrease = () => {
        dispatch(increaseMatrixSize())
    }
    const handleDecrease = () => {
        dispatch(decreaseMatrixSize())
    }

    return (
        <div className="App">
            <div>Zmień rozmiar tablicy</div>
            <nav>
                <button onClick={handleIncrease}>+</button>
                <button onClick={handleDecrease}>-</button>
            </nav>
            <div className='matrix-size'>
                {matrix.matrixSize}x{matrix.matrixSize}
                {matrix.matrixSize === 4 && <b> Minimalny rozmiar tablicy</b>}
            </div>

            <div className='matrix'>
                {
                    matrix.matrix.map((rowData, keyRow) => <Row rowDataArray={rowData} keyRow={keyRow}
                                                                rowSum={matrix.matrixRowSum[keyRow]}/>)
                }
            </div>

            <div className='sum-box'>
                Suma całościowa: <b>{matrix.matrixRowSum.reduce((partialSum, a) => partialSum + a, 0)}</b>
            </div>
        </div>
    );
}

export default App;
